-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2020 at 04:42 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `c19_sss_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) NOT NULL,
  `age` int(4) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `temperature` float(10,2) NOT NULL,
  `assessment_date` date NOT NULL,
  `score` int(11) NOT NULL,
  `result` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `age`, `sex`, `temperature`, `assessment_date`, `score`, `result`) VALUES
(1, 45, 'M', 45.00, '2020-09-30', 9, 'Positive'),
(2, 34, 'F', 104.00, '2020-09-30', 9, 'Positive'),
(3, 32, 'M', 102.00, '2020-09-30', 4, 'Negative'),
(4, 45, 'F', 104.00, '2020-09-30', 5, 'Positive'),
(5, 12, 'M', 104.00, '2020-10-01', 5, 'Positive'),
(7, 34, 'M', 105.00, '2020-10-01', 8, 'Positive'),
(8, 23, 'F', 103.00, '2020-10-01', 8, 'Positive'),
(9, 34, 'F', 101.00, '2020-10-01', 6, 'Positive'),
(10, 12, 'F', 104.00, '2020-10-01', 10, 'Positive'),
(11, 45, 'F', 99.60, '2020-10-02', 6, 'Positive');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
