/**
 * Created by HEMEL on 10/2/2020.
 */

$('#step2Form').submit( function () {
    var isValid = true;
    var checkNone = true;
    var checkboxes = document.getElementsByClassName('symptom');
    var noSymptom = document.getElementById('none');

    for(var i=0; i<checkboxes.length; i++) {
        if(checkboxes[i].checked){
            checkNone = false;
            break;
        }
    }
    /*if none of the valid system is selected also none option is selected*/
    if(checkNone && !noSymptom.checked) {
        $('#error').html("* You can't select any more symptom if you chose no symptom!");
        $('form').addClass('text-danger');
        return false;
    }

    if(noSymptom.checked) {
        var symptoms = document.getElementsByClassName('symptom');
        for(var i=0; i<symptoms.length; i++) {
            if(symptoms[i].checked) {
                $('#error').html("* You can't select any more symptom if you chose no symptom!");
                $('form').addClass('text-danger');
                isValid = false;
                break;
            }
        }
    }
    if(isValid) {
        alert("Information are valid and proceed to the next step?");
    }
    return isValid;
});
$('input').click(function () {
    $('form').removeClass('text-danger');
    $('#error').html('');
});