/**
 * Created by HEMEL on 10/2/2020.
 */

$('#age').blur ( function () {
    var age = this.value;
    if(age>0 && age<=150) {
        $('#age').removeClass('is-invalid');
        $('#age').addClass('is-valid');
    }
    else {
        $('#age').removeClass('is-valid');
        $('#age').addClass('is-invalid');
    }
});
$('#age').keyup( function () {
    var age = this.value;
    if(age>0 && age<=150) {
        $('#age').removeClass('is-invalid');
        $('#age').addClass('is-valid');
    }
    else {
        $('#age').removeClass('is-valid');
        $('#age').addClass('is-invalid');
    }
});
$('#temperature').blur( function () {
    var temperature = this.value;
    if(temperature>50 && temperature<=120) {
        $('#temperature').removeClass('is-invalid');
        $('#temperature').addClass('is-valid');
    }
    else {
        $('#temperature').removeClass('is-valid');
        $('#temperature').addClass('is-invalid');
    }
});
$('#temperature').keyup( function () {
    var temperature = this.value;
    if(temperature>50 && temperature<=120) {
        $('#temperature').removeClass('is-invalid');
        $('#temperature').addClass('is-valid');
    }
    else {
        $('#temperature').removeClass('is-valid');
        $('#temperature').addClass('is-invalid');
    }
});

$('#step1From').submit( function () {
    var isValid = false;
    var age = document.getElementById('age').value;
    var sex = document.getElementsByName('sex');
    var temperature = document.getElementById('temperature').value;
    if(age>0 && age<=150) {
        if(sex[0].checked || sex[1].checked) {
            if(temperature>50 && temperature<=120) {
                isValid = true;
            }
        }
    }
    if(isValid) {
        alert("Information are valid and proceed to the next step?");
    }
    return isValid;
});
