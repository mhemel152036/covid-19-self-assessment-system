<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 10:46 PM
 */

namespace App\classes;


class Database
{
    public function dbConnect() {
        $hostName = 'localhost';
        $userName = 'root';
        $password = '';
        $database = 'c19_sss_db';
        $link = mysqli_connect($hostName, $userName, $password, $database);
        return $link;
    }

}