<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 10:45 PM
 */

namespace App\classes;


class Report
{
    protected function getResult($score) {
        if($score < 5) {
            return 'Negative';
        } else if($score<=5) {
            return 'Positive';
        } else if($score > 5 && $score < 7){
            return 'Positive';
        }else if($score >= 7) {
            return 'Positive';
        }
    }
    protected function getAdvice ($score) {
        if($score < 5) {
            $advice = "Merely have chance to get affected by COVID-19. You should go for isolation and contact doctor and follow advice.";
            return $advice;
        } else if($score<=5) {
            $advice = "Possible suspected case for COVID-19 affected. You should go for isolation and contact doctor and follow advice.";
            return $advice;
        } else if($score > 5 && $score < 7) {
            $advice = "Highly chance of COVID-19 affected. You You advised to go for isolation for isolation and contact doctor immediately and follow advice. List of emergency number available to contact in case of any emergency: 
                      <ul>
                         <li><b>IEDCR:</b> 10655 and 01944333222</li>
                         <li><b>IEDCR Email:</b> iedcrcovid19@gmail.com</li>
                         <li><b>Shasthya Batayon hotline:</b> 16263</li>
                         <li><b>DGHS Hotline:</b> 333</li>
                         <li><b>Armed Forces contact:</b> 01769045739</li>
                      </ul>";
            return $advice;
        } else if($score >= 7) {
            $advice = "Almost confirmed case of COVID-19 positive. You advised to go for isolation and contact doctor immediately and flow advice. Highly advice to be hospitalized. List of emergency number available to contact in case of any emergency: 
                       <ul>
                         <li><b>IEDCR:</b> 10655 and 01944333222</li>
                         <li><b>IEDCR Email:</b> iedcrcovid19@gmail.com</li>
                         <li><b>Shasthya Batayon hotline:</b> 16263</li>
                         <li><b>DGHS Hotline:</b> 333</li>
                         <li><b>Armed Forces contact:</b> 01769045739</li>
                      </ul>";
            return $advice;
        }
    }
    protected function redirectToReport($total_score, $result, $advice) {
        session_start();
        $_SESSION['score']  = $total_score;
        $_SESSION['result'] = $result;
        $_SESSION['advice'] = $advice;
        header('Location: ../reports/report.php');
    }
    public function generateReport() {
        session_start();

        $total_score = $_SESSION['temperature_score'] + $_SESSION['step2_score'] + $_SESSION['step3_score'];
        $date = date('Y-m-d');
        $result = $this->getResult($total_score);

        $link = Database::dbConnect();
        $sql = "INSERT INTO reports (age,sex,temperature,assessment_date,score,result) VALUES ('$_SESSION[age]','$_SESSION[sex]','$_SESSION[temperature]','$date','$total_score','$result')";

        if(mysqli_query($link , $sql)) {
            session_destroy();
            $this->redirectToReport($total_score, $result, $this->getAdvice($total_score));
        }
        else{
            die("Query problem ".mysqli_error($link));
        }
    }
    public function getAllReports() {
        $link = Database::dbConnect();
        $sql = "SELECT * FROM reports";
        if(mysqli_query($link ,$sql)) {
            $queryResult = mysqli_query($link, $sql);
            return $queryResult;
        }else {
            die("Query problem ".mysqli_error($link));
        }
    }

}