<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 10:00 PM
 */

session_start();
$score = $_SESSION['score'];
$advice = $_SESSION['advice'];
$result = $_SESSION['result'];
if(!$score && !$advice && !$result) {
    header('Location: ../servey/servey-1.php');
}
session_destroy();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Individual report</title>
    <!-- Bootstrap core CSS -->
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>

<body>
<?php include_once "../includes/header.php" ?>

<section class="mh-450 my-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title text-center">Report</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-3 lead">
                                Total Score:
                            </div>
                            <div class="col-12 col-md-9">
                                <?php echo $score; ?>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12 col-md-3 lead">
                                Advice:
                            </div>
                            <div class="col-12 col-md-9 text-justify">
                                <?php echo $advice; ?>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12 col-md-3 lead">
                                Result:
                            </div>
                            <div class="col-12 col-md-9">
                                <?php if($result == 'Positive') { ?>
                                    <h4 class="text-danger"><?php echo $result ? 'COVID-19 '.$result : ''; ?></h4>
                                <?php }else{ ?>
                                    <h4 class="text-success"><?php echo $result ? 'COVID-19 '.$result : ''; ?></h4>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once "../includes/footer.php" ?>
<!-- Bootstrap core JavaScript -->
<script src="../../assets/jquery/jquery.slim.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/js/script.js"></script>
</body>
</html>
