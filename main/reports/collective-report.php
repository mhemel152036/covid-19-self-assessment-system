<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 10:01 PM
 */
require_once '../../vendor/autoload.php';
$report = new \App\classes\Report();
$reports = $report->getAllReports();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Collective report</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>

<body>

<?php include_once "../includes/header.php"; ?>

<section class="mh-450 my-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center card-title">Collective Report</h3>
                    </div>
                    <div class="card-body mt-3">
                        <table class="table table-hover text-center table-responsive-lg">
                            <thead>
                            <tr>
                                <th>Sl No.</th>
                                <th>Age</th>
                                <th>Sex</th>
                                <th>Temperature</th>
                                <th>Assessment Date</th>
                                <th>Assessment Score</th>
                                <th>COVID-19 Result</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; while($report = mysqli_fetch_assoc($reports)) { ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $report['age']; ?></td>
                                    <td><?php echo $report['sex']; ?></td>
                                    <td><?php echo $report['temperature']; ?></td>
                                    <td><?php echo $report['assessment_date']; ?></td>
                                    <td><?php echo $report['score']; ?></td>
                                    <td><?php echo $report['result']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include_once "../includes/footer.php"; ?>

<!-- Bootstrap core JavaScript -->
<script src="../../assets/jquery/jquery.slim.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/js/script.js"></script>

</body>

</html>

