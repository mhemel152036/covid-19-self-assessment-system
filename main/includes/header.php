<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light static-top">
    <div class="container">
        <a class="navbar-brand" href="../../index.php">
            <img class="logo" src="../../assets/image/unnamed.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../../index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../servey/servey-1.php">Self Assessment</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../reports/collective-report.php">Collective Report</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
