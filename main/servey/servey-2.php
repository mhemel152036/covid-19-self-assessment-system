<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 9:42 PM
 */

session_start();

if($_SESSION['age'] && $_SESSION['sex'] && $_SESSION['temperature']) {
    if(isset($_POST['next'])) {
        if(isset($_POST['none'])) {
            $_SESSION['step2_score'] = 0;
        } else {
            $length = count($_POST);
            $score = 3;
            for ($i = 0; $i < $length - 2; $i++) {
                $score = $score + 1;
            }
            $_SESSION['step2_score'] = $score;
        }
        header('Location: servey-3.php');
    }
    else if(isset($_POST['prev'])) {
        session_destroy();
        header('Location: servey-1.php');
    }
}
else {
    header('Location: servey-1.php'); /* if servey1 criteria is not full filled */
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Servey</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>

<body>

<!-- Navigation -->
<?php include_once "../includes/header.php"; ?>

<section class="my-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Step 2:</h3>
                    </div>
                    <div class="card-body ">
                        <form id="step2Form" method="POST" action="">
                            <small id="error" class="text-danger"></small>
                            <fieldset>
                                <legend>Do you have any of the below symptoms?</legend>
                                <div class="form-check mb-1 mt-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom1" value="Breathing problem"> Breathing problem
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom2" value="Dry cough"> Dry cough
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom3" value="Sore throat"> Sore throat
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom4" value="Weakness"> Weakness
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom5" value="Runny nose"> Runny nose
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="none" id="none" value="none"> None of the Above
                                    </label>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9 text-right">
                                    <input class="btn btn-success rounded-0" type="button" onclick="document.getElementById('submitForm').submit();" name="prev" value="Prev">

                                    <input class="btn btn-success rounded-0" type="submit" name="next" value="Next">
                                </div>
                            </div>
                        </form>
                        <form id="submitForm" method="POST" action="">
                            <input type="hidden" class="btn btn-success rounded-0" name="prev" value="Prev">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once "../includes/footer.php"; ?>
<!-- Bootstrap core JavaScript -->
<script src="../../assets/jquery/jquery.slim.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Custom Javascript -->
<script src="../../assets/js/script2.js"></script>



</body>

</html>
