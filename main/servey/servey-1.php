<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 9:42 PM
 */
if(isset($_POST['submit'])) {
    session_start();
    $_SESSION['age'] = $_POST['age'];
    $_SESSION['sex'] = $_POST['sex'];
    $_SESSION['temperature'] = $_POST['temperature'];
    if($_POST['temperature'] > 99.5 || $_POST['temperature'] > 100.9){
        $_SESSION['temperature_score'] = 2;
    } else{
        $_SESSION['temperature_score'] = 0;
    }
    header('Location: servey-2.php');

}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Servey</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>

<body>

<?php include_once "../includes/header.php"; ?>

<section class="mh-450 my-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center card-title">Step 1:</h3>
                    </div>
                    <div class="card-body ">
                        <form id="step1From" method="POST" action="">
                            <div class="form-group row">
                                <label for="age" class="col-12 col-sm-4 lead">Age</label>
                                <div class="col-12 col-sm-8">
                                    <input class="form-control rounded-0" type="number" name="age" id="age" min="1" max="150" required>
                                    <small class="invalid-feedback">Invalid age entry</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="age" class="col-12 col-sm-4 lead">Sex</label>
                                <div class="col-12 col-sm-8 is-invalid">
                                    <input class="mr-2" type="radio" name="sex" value="M" checked>Male
                                    <input class="mx-2" type="radio" name="sex" value="F">Female
                                    <small class="invalid-feedback"></small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="temperature" class="col-12 col-sm-4 lead">Body Temperature( &deg;F)</label>
                                <div class="col-12 col-sm-8">
                                    <input class="form-control rounded-0" type="text" name="temperature" id="temperature" min="40" max="120" required>
                                    <small class="invalid-feedback">Invalid temperature value</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 col-sm-4"></div>
                                <div class="col-12 col-sm-8 text-right">
                                    <input class="btn btn-success rounded-0" type="submit" name="submit" value="Next">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once "../includes/footer.php"; ?>
<!-- Bootstrap core JavaScript -->
<script src="../../assets/jquery/jquery.slim.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Custom javascript -->
<script src="../../assets/js/script1.js"></script>
</body>

</html>

