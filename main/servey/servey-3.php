<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 9:42 PM
 */

require_once "../../vendor/autoload.php";
session_start();

if(isset($_SESSION['step2_score'])) {
    if(isset($_POST['next'])) {
        if(isset($_POST['none'])) {
            /* If none of the symptom match */
            $_SESSION['step3_score'] = 0;
        } else {
            $length = count($_POST) - 1;
            $step3_score = $length * 2; /* each individual has a score of 2*/
//            for ($i = 0; $i < $length - 2; $i++) {
//                $additional_score = $additional_score + 2;
//            }
            $_SESSION['step3_score'] = $step3_score;
        }

        $report = new \App\classes\Report();
        $report->generateReport();
    } else if(isset($_POST['prev'])) {
        unset($_SESSION['step2_score']);
        header('Location: servey-2.php');
    }
}
else {
    header('Location: servey-2.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Servey</title>

    <!-- Bootstrap core CSS -->
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>

<body>

<?php include_once "../includes/header.php"; ?>

<section class="my-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Step 3:</h3>
                    </div>
                    <div class="card-body ">
                        <p class="card-text text-info text-center">"You have to provide some additional information for the betterment of the further identification process. Kindly share your valueable inforamtion"</p>
                        <form id="step3Form" method="POST" action="">
                            <small id="error" class="text-danger"></small>
                            <fieldset>
                                <legend>Do you have any of the below additional symptoms?</legend>
                                <div class="form-check mb-1 mt-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom1" value="Abdominal pain"> Abdominal pain
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom2" value="Vomiting"> Vomiting
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom3" value="Diarrhoea"> Diarrhoea
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom4" value="Chest pain or pressure"> Chest pain or pressure
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom5" value="Muscle pain"> Muscle pain
                                    </label>
                                </div>

                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom6" value="Loss of taste or smell"> Loss of taste or smell
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom7" value="Rash on skin, or discoloration of fingers or toes"> Rash on skin, or discoloration of fingers or toes
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input class="symptom" type="checkbox" name="symptom8" value="Loss of speech or movement"> Loss of speech or movement
                                    </label>
                                </div>
                                <div class="form-check mb-1">
                                    <label class="form-check-label">
                                        <input type="checkbox" id="none" name="none" value="none"> None of the above
                                    </label>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-9 text-right">
                                    <input class="btn btn-success rounded-0" type="button"  name='prev' onclick="document.getElementById('submitForm').submit();" value="Prev">
                                    <input class="btn btn-success rounded-0" type="submit" name='next' value="Next">
                                </div>
                            </div>
                        </form>
                        <form id="submitForm" action="" method="POST">
                            <input type="hidden" class="btn btn-success rounded-0" type="submit" name='prev' value="Prev">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once "../includes/footer.php"; ?>

<!-- Bootstrap core JavaScript -->
<script src="../../assets/jquery/jquery.slim.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Custom javascript -->
<script src="../../assets/js/script3.js"></script>
</body>

</html>

