<?php
/**
 * Created by PhpStorm.
 * User: HEMEL
 * Date: 9/30/2020
 * Time: 9:28 PM
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light static-top">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img class="logo" src="assets/image/unnamed.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="main/servey/servey-1.php">Self Assessment</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="main/reports/collective-report.php">Collective Report</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section class="my-3 mh-450">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-deck">
                    <div class="card text-center">
                        <div class="card-body">
                            <h3 class="text-primary">COVID-19 Self-Assessment</h3>
                            <p>Use this self-assessment tool to help determine whether you need to be tested for COVID-19. You can complete this assessment for yourself or on behalf of someone else, if they are not able.</p>
                            <a class="btn btn-primary rounded-0" href="main/servey/servey-1.php"> <span class="text-light">Launch self-assessment</span></a>
                        </div>
                    </div>
                    <div class="card text-center">
                        <div class="card-body">
                            <h3 class="text-primary">COVID-19 Collective Report</h3>
                            <p>Use this collective report view tool to show details of COVID-19 situation in a tabular way. This tool will show the number of total COVID-19 positive or negative of different ages and genders.</p>
                            <a class="btn btn-primary rounded-0" href="main/reports/collective-report.php"> <span class="text-light">Launch collective report</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="mt-5 py-5 bg-dark text-light">
    <div class="container text-center">
        <h3 class="text-danger">Disclaimer: </h3>
        <p class="m-0 text-center">"This COVID-19 Self Assessment System is only for software development purpose and may not yeild actual result. Any information given by users of this system will not be disclosed or store to anywhere!"</p>
    </div>
    <!-- /.container -->
</footer>
<!-- Bootstrap core JavaScript -->
<script src="assets/jquery/jquery.slim.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>

